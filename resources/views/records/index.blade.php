@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @foreach ($records as $record)
                        <table>
                            <tr>
                                <th>ID</th>
                                <th>Amount Spend</th>
                                <th>Type</th>
                            </tr>
                            <tr>
                                <td class="text-primary"><strong>{{ $record->id }}</strong></td>
                                <td>{{ $record->amount }}</td>
                                <td>{{ $record->type }}</td>
                            </tr>
                        </table>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection