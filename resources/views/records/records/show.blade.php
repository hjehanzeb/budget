@extends('template')

@section('main')
    <div class="container">
        <div class="row">
            
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Record {{ $record->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/records') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/records/' . $record->id . '/edit') }}" title="Edit Record"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['records', $record->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Record',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $record->id }}</td>
                                    </tr>
                                    <tr><th> Amount </th><td> {{ $record->amount }} </td></tr><tr><th> Type </th><td> {{ $record->type }} </td></tr><tr><th> Payment Type </th><td> {{ $record->payment_type }} </td></tr>
                                    <tr><th> Note </th><td> {{ $record->note }}</td></tr> <tr><th> Date </th><td> {{ $record->date }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
