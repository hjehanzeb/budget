@extends('layouts.app')

@section('head')
{!! Html::script('https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js') !!}
{!! Html::script('https://code.jquery.com/ui/1.12.1/jquery-ui.js') !!}
<script type="text/javascript">
var subCategoriesUrl = '{{ url('/sub-categories') }}';
var noteMatchingUrl = '{{ url('/matching-notes') }}';
</script>
{!! Html::script('js/record-edit.js') !!}
{!! Html::style('https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css') !!}
@endsection

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Edit Record #{{ $record->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/records') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($record, [
                            'method' => 'PATCH',
                            'url' => ['/records', $record->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('records.records.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
