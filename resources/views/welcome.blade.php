@extends('template')

@section('main')
    <div class="title m-b-md">
        Hello :)
    </div>

    <div class="links">
        <a href="{{ url('/records') }}">Records</a>
        <a href="{{ url('/categories') }}">Categories</a>
        @if (Auth::check())
            <a href="{{ url('/home') }}">Home</a>
        @else
            <a href="{{ url('/login') }}">Login</a>
        @endif
    </div>
@endsection