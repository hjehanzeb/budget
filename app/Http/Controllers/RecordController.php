<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Record;
use App\Models\Account;
use App\Models\Currency;
use App\Models\Category;
use Illuminate\Http\Request;
use Carbon\Carbon;

class RecordController extends Controller
{
    /**
     * Create a new RecordController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('index', 'store', 'edit', 'create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request, $filter = null)
    {
        $keyword = $request->get('search');
        $perPage = 20;

        if (!empty($keyword)) {
            $records = Record::where('amount', 'LIKE', "%$keyword%")
                ->orWhere('type', 'LIKE', "%$keyword%")
                ->orWhere('payment_type', 'LIKE', "%$keyword%")
                ->orWhere('date', 'LIKE', "%$keyword%")
                ->orWhere('payee', 'LIKE', "%$keyword%")
                ->orWhere('labels', 'LIKE', "%$keyword%")
                ->orWhere('account_id', 'LIKE', "%$keyword%")
                ->orWhere('category_id', 'LIKE', "%$keyword%")
                ->orWhere('currency_id', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            if (!empty($filter)) {
                switch ($filter) {
                    case 'month':
                        $records = Record::whereMonth('date', Carbon::today()->format('m'))->paginate($perPage);
                        break;
                    case 'week':
                        $records = Record::whereWeek('date', Carbon::today()->format('W'))->paginate($perPage);
                        break;
                    case 'today':
                        $records = Record::whereDate('date', Carbon::today()->toDateString())->paginate($perPage);
                        break;
                    default:
                        break;
                }
            } else {
                $records = Record::paginate($perPage);
            }
        }

        return view('records.records.index', compact('records', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        list($accounts, $main_categories, $currencies) = $this->getFormViewRelatedContent();

        // {!! Form::select('items', $items, null, ) !!}
        return view('records.records.create', compact('accounts', 'main_categories', 'currencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required'
        ]);
        $requestData = $request->all();
        $requestData['date'] .= ' ' . $requestData['time_part'] . ':00';
        
        Record::create($requestData);

        return redirect('records')->with('flash_message', 'Record added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $record = Record::findOrFail($id);

        return view('records.records.show', compact('record'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $record = Record::findOrFail($id);

        list($accounts, $main_categories, $currencies) = $this->getFormViewRelatedContent();

        return view('records.records.edit', compact('record', 'accounts', 'main_categories', 'currencies'));
    }

    protected function getFormViewRelatedContent()
    {
        $accounts = Account::pluck('name', 'id');
        $categories = Category::where('parent_category_id', null)->pluck('name', 'id');
        $currencies = Currency::pluck('name', 'id');

        return [$accounts, $categories, $currencies];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'amount' => 'required'
        ]);
        $requestData = $request->all();
        
        $record = Record::findOrFail($id);
        $record->update($requestData);

        return redirect('records')->with('flash_message', 'Record updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Record::destroy($id);

        return redirect('records')->with('flash_message', 'Record deleted!');
    }

    /**
     * For geting matching notes for autocomplete
     *
     * @param  Request $request    Request
     * @param  string  $note_query Note query to be search in the database
     * @return string              JSON array of all matching notes
     */
    public function getMatchingNotes(Request $request)
    {
        $matching_notes = Record::where('note', 'LIKE', "%{$request->input('term')}%")->distinct('note')->pluck('note');

        return response()->json($matching_notes);
    }
}
