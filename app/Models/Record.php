<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 06 Mar 2018 15:50:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use App\Models\Category;
use App\Models\Account;
use App\Models\Currency;

/**
 * Class Record
 * 
 * @property int $id
 * @property int $account_id
 * @property int $category_id
 * @property int $currency_id
 * @property float $amount
 * @property string $type
 * @property string $payment_type
 * @property string $note
 * @property \Carbon\Carbon $date
 * @property float $gps_latitude
 * @property float $gps_longitude
 * @property string $payee
 * @property string $labels
 *
 * @package App\Models
 */
class Record extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'account_id' => 'int',
		'category_id' => 'int',
		'currency_id' => 'int',
		'amount' => 'float',
		'gps_latitude' => 'float',
		'gps_longitude' => 'float'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'account_id',
		'category_id',
		'currency_id',
		'amount',
		'type',
		'payment_type',
		'note',
		'date',
		'gps_latitude',
		'gps_longitude',
		'payee',
		'labels'
	];

	/**
     * One to Many relation from Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * One to Many relation from Account
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    /**
     * One to Many relation from Currency
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
}
