<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 06 Mar 2018 15:50:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use App\Models\Record;

/**
 * Class Category
 * 
 * @property int $id
 * @property int $parent_category_id
 * @property string $name
 * @property string $category_path
 * @property \Carbon\Carbon $created_date
 *
 * @package App\Models
 */
class Category extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'parent_category_id' => 'int'
	];

	protected $dates = [
		'created_date'
	];

	protected $fillable = [
		'parent_category_id',
		'name',
		'category_path',
		'created_date'
	];

	/**
     * One to Many relation with Records
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function records()
    {
        return $this->hasMany(Record::class);
    }
}
