<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 06 Mar 2018 15:50:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 * 
 * @property int $id
 * @property string $username
 * @property string $password
 * @property \Carbon\Carbon $last_login
 * @property \Carbon\Carbon $created_date
 *
 * @package App\Models
 */
class User extends Eloquent
{
	public $timestamps = false;

	protected $dates = [
		'last_login',
		'created_date'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'username',
		'password',
		'last_login',
		'created_date'
	];
}
