<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 06 Mar 2018 15:50:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use App\Models\Record;

/**
 * Class Currency
 * 
 * @property int $id
 * @property string $name
 * @property string $code
 * @property \Carbon\Carbon $created_date
 *
 * @package App\Models
 */
class Currency extends Eloquent
{
	public $timestamps = false;

	protected $dates = [
		'created_date'
	];

	protected $fillable = [
		'name',
		'code',
		'created_date'
	];

    /**
     * One to Many relation with Records
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function records()
    {
        return $this->hasMany(Record::class);
    }
}
