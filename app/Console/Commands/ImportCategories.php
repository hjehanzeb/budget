<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Category;
use App\Models\Record;

class ImportCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:import-categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'For importing categories in db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cols = array(
            'account','category','currency','amount','ref_currency_amount','type','payment_type','payment_type_local','note','date','gps_latitude','gps_longitude','gps_accuracy_in_meters','warranty_in_month','transfer','payee','labels','envelope_id','custom_category'
        );
        $rev_cols = array_reverse($cols);

        $account_trans = ['My Account' => '1', 'Extras' => '2'];
        $type_trans = ['Expenses' => 'expense', 'Income' => 'income'];
        $currency_trans = ['PKR' => '1', 'USD' => '2'];
        $category_trans = array(
            'Food & Drinks' => 'Food & Drinks > Food & Drinks',
            'Home, garden' => 'Shopping > Home, garden',
            'Charges, Fees' => 'Financial Expenses > Charges, Fees',
            'Collections' => 'Investments > Collections',
            'Phone, cell phone' => 'Communication, PC > Phone, cell phone',
            'Internet' => 'Communication, PC > Internet',
            'Drug-store, chemist' => 'Shopping > Drug-store, chemist',
            'Pocket Money' => 'Financial Expenses > Pocket Money',
            'Medicine' => 'Life & Entertainment > Medicine',
            'Groceries' => 'Food & Drinks > Groceries',
            'Bills' => 'Housing > Energy, Utility Bills',
            'Government Registration Charges' => 'Life & Entertainment > Government Registration Charges',
            'Health care, doctor' => 'Life & Entertainment > Health care, doctor',
            'Charity, gifts' => 'Life & Entertainment > Charity, gifts',
            'Clothes & shoes' => 'Shopping > Clothes & shoes',
            'Wage, invoices' => 'Income > Wage, invoices',
            'Software, apps, games' => 'Communication, PC > Software, apps, games',
            'Wellness, beauty' => 'Life & Entertainment > Wellness, beauty',
            'Fuel' => 'Vehicle > Fuel',
            'Services' => 'Transportation > Courier Services',
            'Vehicle maintenance' => 'Vehicle > Vehicle maintenance',
            'Restaurant, fast-food' => 'Food & Drinks > Restaurant, fast-food',
            'Toys' => 'Shopping > Toys',
            'Gifts, joy' => 'Shopping > Gifts, joy',
            'Vehicle Installment' => 'Vehicle > Vehicle Installment',
            'Stationery, tools' => 'Shopping > Stationery, tools',
            'Holiday, trips, hotels' => 'Life & Entertainment > Holiday, trips, hotels',
            'Electronics, accessories' => 'Shopping > Electronics, accessories',
            'Long distance' => 'Transportation > Long distance',
            'Taxi' => 'Transportation > Taxi',
            'Transportation' => 'Transportation > Transportation (Others)',
            'Parking' => 'Vehicle > Parking',
            'Advisory' => 'Financial expenses > Advisory',
            'Life events' => 'Life & Entertainment > Life events',
            'Hardware Accessories' => 'Housing > Hardware Accessories',
            'Health and beauty' => 'Life & Entertainment > Health and beauty',
            'Kids' => 'Shopping > Kids',
            'Energy, utilities' => 'Housing > Energy, utilities',
            'Books, audio, subscriptions' => 'Life & Entertainment > Books, audio, subscriptions',
            'Life & Entertainment' => 'Life & Entertainment > Life & Entertainment (Others)',
            'Loan, interests' => 'Financial expenses > Loan, interests',
            'Income' => 'Income > Income (Others)',
            'Jewels, accessories' => 'Shopping > Jewels, accessories',
            'Public transport' => 'Transportation > Public transport',
            'Taxes' => 'Financial expenses > Taxes',
            'Shopping' => 'Shopping > Shopping (Others)',
            'Education, development' => 'Life & Entertainment > Education, development',
            'Maintenance, repairs' => 'Housing > Maintenance, repairs',
            'Others' => 'Others',
            'Financial expenses' => 'Financial expenses > Financial expenses (Others)',
            'Fines' => 'Financial Expenses > Fines',
            'TV, Streaming' => 'Life & Entertainment > TV, Streaming',
        );

        // Inserting categories
        foreach ($category_trans as $category => $path) {
            $path_explode = explode(' > ', $path);
            $parent_cat = $path_explode[0];
            $sub_category = (isset($path_explode[1]) ? $path_explode[1] : '');

            // Check if Parent Category exist
            $category_obj = Category::where('name', $parent_cat)->where('category_path', null)->first();
            if (empty($category_obj->id)) {
                $category_obj = new Category;
                $category_obj->parent_category_id = null;
                $category_obj->name = $parent_cat;
                $category_obj->category_path = null;
                $category_obj->created_date = date('Y-m-d H:i:s');

                $category_obj->save();
            }

            if (!empty($sub_category)) {
                $sub_category_obj = new Category;
                $sub_category_obj->parent_category_id = $category_obj->id;
                $sub_category_obj->name = $sub_category;
                $sub_category_obj->created_date = date('Y-m-d H:i:s');

                $sub_category_obj->save();

                $sub_category_obj->category_path = "{$category_obj->id}/{$sub_category_obj->id}";
                $sub_category_obj->save();
            }
        }
    }
}












