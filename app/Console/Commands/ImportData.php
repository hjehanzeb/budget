<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use App\Models\Category;
use App\Models\Record;
use App\Models\Currency;
use App\Models\Account;
use Maatwebsite\Excel\Facades\Excel;

class ImportData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:import-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'For importing data in db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cols = array(
            'account','category','currency','amount','ref_currency_amount','type','payment_type','payment_type_local','note','date','gps_latitude','gps_longitude','gps_accuracy_in_meters','warranty_in_month','transfer','payee','labels','envelope_id','custom_category'
        );
        $rev_cols = array_flip($cols);
        $cat_trans = [
            'Bills' => 'Energy, Utility Bills',
            'Income' => 'Income (Others)',
            'Transportation' => 'Transportation (Others)',
            'Life & Entertainment' => 'Life & Entertainment (Others)',
            'Services' => 'Courier Services',
            'Shopping' => 'Shopping (Others)',
            'Financial expenses' => 'Financial expenses (Others)'
        ];
        
        $this->info('Going to load the file');
        $csv = fopen('/var/www/html/budget/report_2018-03-12_4-17pm.csv', 'r');
        $first = true;
        while (($csv_line = fgetcsv($csv, 940, ";")) !== false) {
            $this->info('Category: '.print_r($csv_line[$rev_cols['category']], 1).'; Date: '.$csv_line[$rev_cols['date']]);
            if ($first) {
                $first = !$first;
                continue;
            }

            // Translating Category if needed
            if (array_key_exists($csv_line[$rev_cols['category']], $cat_trans)) {
                $csv_line[$rev_cols['category']] = $cat_trans[$csv_line[$rev_cols['category']]];
            }

            $category_obj = Category::where('name', $csv_line[$rev_cols['category']])
                ->where('parent_category_id', '!=', null)
                ->first();
            $currency_obj = Currency::where('name', $csv_line[$rev_cols['currency']])->first();
            $account_obj = Account::where('name', $csv_line[$rev_cols['account']])->first();

            if (!empty($category_obj->id)) {
                Record::create([
                    'amount' => $csv_line[$rev_cols['amount']],
                    'category_id' => $category_obj->id,
                    'account_id' => $account_obj->id,
                    'currency_id' => $currency_obj->id,
                    'type' => strtolower($csv_line[$rev_cols['type']]),
                    'payment_type' => (empty($csv_line[$rev_cols['payment_type']])) ?
                        'cash' : str_replace('_', ' ', strtolower($csv_line[$rev_cols['payment_type']])),
                    'note' => $csv_line[$rev_cols['note']],
                    'date' => $csv_line[$rev_cols['date']],
                    'gps_latitude' => (empty($csv_line[$rev_cols['gps_latitude']]) ? null : $csv_line[$rev_cols['gps_latitude']]),
                    'gps_longitude' => (empty($csv_line[$rev_cols['gps_longitude']]) ? null : $csv_line[$rev_cols['gps_longitude']]),
                    'payee' => (empty($csv_line[$rev_cols['payee']]) ? null : $csv_line[$rev_cols['payee']]),
                    'labels' => (empty($csv_line[$rev_cols['labels']]) ? null : $csv_line[$rev_cols['labels']])
                ]);
            } else {
                $this->error("Category not found for {$csv_line[$rev_cols['date']]}");
            }
        }
        fclose($csv);
    }
}
