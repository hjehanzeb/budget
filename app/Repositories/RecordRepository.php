<?php

namespace App\Repositories;

use App\Models\Record;

class RecordRepository extends BaseRepository
{
    /**
     * Create a new RecordRepository instance.
     *
     * @param  \App\Models\Record $record
     * @return void
     */
    public function __construct(Record $record)
    {
        $this->model = $record;
    }

    public function getAllRecords($pagination = false, $default_page_size = 20)
    {
        if ($pagination) {
            return $this->model->paginate($default_page_size);
        }

        return $this->model->get();
    }
}