<?php

namespace App\Repositories;

use App\Models\Account;

class AccountRepository extends BaseRepository
{
    /**
     * Create a new AccountRepository instance.
     *
     * @param  \App\Models\Account $account
     * @return void
     */
    public function __construct(Account $account)
    {
        $this->model = $account;
    }

    /**
     * Create or update a post.
     *
     * @param  \App\Models\Account $account
     * @param  array  $inputs
     * @return \App\Models\Account
     */
    protected function saveAccount($account, $inputs)
    {
        $account->name = $inputs['name'];
        $account->save();

        return $account;
    }

    /**
     * Update an Account.
     *
     * @param  array  $inputs
     * @param  \App\Models\Account $post
     * @return void
     */
    public function update($inputs, $account)
    {
        $account = $this->saveAccount($account, $inputs);
    }

    /**
     * Destroy an account.
     *
     * @param  \App\Models\Account $account
     * @return void
     */
    public function destroy($account)
    {
        $account->tags()->detach();

        $account->delete();
    }
}
