$j = jQuery.noConflict();

$j(document).ready(function (argument) {
    $j('[name="main_categories"]').change(function() {
        var catId = $j('[name="main_categories"] option:selected').attr('value');
        $j.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $j('meta[name="csrf-token"]').attr('content')
            }
        });

        $j.ajax({
            url: subCategoriesUrl + '/' + catId,
            method: "POST",
            beforeSend: function( xhr ) {
                // xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
                $j('[name="category_id"]').html('');
            }
        }).done(function( data ) {
            if ( console && console.log ) {
                for (var subCategory in data) {
                    console.log('subcategory name: ', data[subCategory].name);
                }
            }
            for (var subCategory in data) {
                $j('[name="category_id"]').append('<option value="'+ data[subCategory].id +'">'+ data[subCategory].name +'</option>');
            }
        });
    });

    $j('#amount').focusout(function () {
        if ($j('#type').val() == 'expenses') {
            $j(this).val(Math.abs(parseFloat($j(this).val())) * -1);
        } else {
            $j(this).val(Math.abs(parseFloat($j(this).val())));
        }
    });

    $j('#type').change(function () {
        if ($j('#type option:selected').attr('value') == 'expenses') {
            $j('#amount').val(Math.abs(parseFloat($j('#amount').val())) * -1);
        } else {
            $j('#amount').val(Math.abs(parseFloat($j('#amount').val())));
        }
    });

    $j('#note').autocomplete({
        source: noteMatchingUrl,
        minLength: 2,
        select: function( event, ui ) {
            console.log(ui.item);
        }
    });
});
