<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('categories', 'CategoryController');
Route::resource('records', 'RecordController');
Route::get('record/{filter}', 'RecordController@index');
Route::resource('category', 'CategoryController');
Route::post('sub-categories/{category_id}', 'CategoryController@getSubCategories');
Route::get('matching-notes', 'RecordController@getMatchingNotes');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
