<?php

$cols = array(
    'account','category','currency','amount','ref_currency_amount','type','payment_type','payment_type_local','note','date','gps_latitude','gps_longitude','gps_accuracy_in_meters','warranty_in_month','transfer','payee','labels','envelope_id','custom_category'
);
$rev_cols = array_reverse($cols);

$account_trans = ['My Account' => '1', 'Extras' => '2'];
$type_trans = ['Expenses' => 'expense', 'Income' => 'income'];
$currency_trans = ['PKR' => '1', 'USD' => '2'];
$category_trans = array(
    'Food & Drinks' => 'Food & Drinks > Food & Drinks',
    'Home, garden' => 'Shopping > Home, garden',
    'Charges, Fees' => 'Financial Expenses > Charges, Fees',
    'Collections' => 'Investments > Collections',
    'Phone, cell phone' => 'Communication, PC > Phone, cell phone',
    'Internet' => 'Communication, PC > Internet',
    'Drug-store, chemist' => 'Shopping > Drug-store, chemist',
    'Pocket Money' => 'Financial Expenses > Pocket Money',
    'Medicine' => 'Life & Entertainment > Medicine',
    'Groceries' => 'Food & Drinks > Groceries',
    'Bills' => 'Housing > Energy, Utility Bills',
    'Government Registration Charges' => 'Life & Entertainment > Government Registration Charges',
    'Health care, doctor' => 'Life & Entertainment > Health care, doctor',
    'Charity, gifts' => 'Life & Entertainment > Charity, gifts',
    'Clothes & shoes' => 'Shopping > Clothes & shoes',
    'Wage, invoices' => 'Income > Wage, invoices',
    'Software, apps, games' => 'Communication, PC > Software, apps, games',
    'Wellness, beauty' => 'Life & Entertainment > Wellness, beauty',
    'Fuel' => 'Vehicle > Fuel',
    'Services' => 'Transportation > Courier Services',
    'Vehicle maintenance' => 'Vehicle > Vehicle maintenance',
    'Restaurant, fast-food' => 'Food & Drinks > Restaurant, fast-food',
    'Toys' => 'Shopping > Toys',
    'Gifts, joy' => 'Shopping > Gifts, joy',
    'Vehicle Installment' => 'Vehicle > Vehicle Installment',
    'Stationery, tools' => 'Shopping > Stationery, tools',
    'Holiday, trips, hotels' => 'Life & Entertainment > Holiday, trips, hotels',
    'Electronics, accessories' => 'Shopping > Electronics, accessories',
    'Long distance' => 'Transportation > Long distance',
    'Taxi' => 'Transportation > Taxi',
    'Transportation' => 'Transportation > Transportation (Others)',
    'Parking' => 'Vehicle > Parking',
    'Advisory' => 'Financial expenses > Advisory',
    'Life events' => 'Life & Entertainment > Life events',
    'Hardware Accessories' => 'Housing > Hardware Accessories',
    'Health and beauty' => 'Life & Entertainment > Health and beauty',
    'Kids' => 'Shopping > Kids',
    'Energy, utilities' => 'Housing > Energy, utilities',
    'Books, audio, subscriptions' => 'Life & Entertainment > Books, audio, subscriptions',
    'Life & Entertainment' => 'Life & Entertainment > Life & Entertainment (Others)',
    'Loan, interests' => 'Financial expenses > Loan, interests',
    'Income' => 'Income > Income (Others)',
    'Jewels, accessories' => 'Shopping > Jewels, accessories',
    'Public transport' => 'Transportation > Public transport',
    'Taxes' => 'Financial expenses > Taxes',
    'Shopping' => 'Shopping > Shopping (Others)',
    'Education, development' => 'Life & Entertainment > Education, development',
    'Maintenance, repairs' => 'Housing > Maintenance, repairs',
    'Others' => 'Others',
    'Financial expenses' => 'Financial expenses > Financial expenses (Others)',
    'Fines' => 'Financial Expenses > Fines',
    'TV, Streaming' => 'Life & Entertainment > TV, Streaming',
);

$csv = fopen('report_2018-03-12_4-17pm.csv', 'r');
while (($data = fgetcsv($handle, 940, ",")) !== false) {
    
}
fclose($handle);
